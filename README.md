# time_series_community_detection

Example scripts to perform clustering and community detection.

* Adapted from

    * https://github.com/lnferreira/time_series_clustering_via_community_detection
    
* R folder

    * Install using installation_script.R
    
    * Run using run.R
    
